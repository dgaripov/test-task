Test task
========================

## Installation

- clone repository
- `composer install`
- `bin/console doctrine:schema:create`
- go to the index page to seed the database with data
- follow the next links:
    - `domain.dev/widget/{UUID}.xml`
    - `domain.dev/widget/{UUID}.json`
    - `domain.dev/widget/{UUID}.html`
    - `domain.dev/widget/{UUID}.png`
    
